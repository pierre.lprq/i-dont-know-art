﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneMngr : MonoBehaviour
{
    public string NextScene;
    public int cptr;
    private bool compteur;

    void Awake()
    {
        compteur = false;
    }

    void Update()
    {
        //Debug.Log("Cptr = " + cptr);

        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }

        if (Input.anyKey)
        {
            compteur = true;
        }

        if (compteur == true)
        {
            if (cptr > 0)
            {
                cptr--;
            }

            else
            {
                SceneManager.LoadScene("Scenes/" + NextScene);
            }
        }

    }
}
